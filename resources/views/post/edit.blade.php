<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
    .wrapper--w680 {
        max-width: 680px;
    }
    .wrapper {
        margin: 0 auto;
    }
</style>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Post') }}
        </h2>
    </x-slot>
    <div class="container">
        @include('_messages')
        <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
            <div class="wrapper wrapper--w680">
                <div class="card card-4 mt-10">
                    <div class="card-body">
                        <form method="post" action="{{ URL::route('post.update',$post) }}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            @method('PUT')
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp"
                                       placeholder="Enter title" value="{{old('title',$post->title)}}">
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" id="description" rows="3">{{old('description',$post->description)}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Photo</label>
                                <div class="col-md-4 mb-2">
                                    <img src="{{$post->image_url}}" class="card-img" alt="...">
                                </div>
                                <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>
