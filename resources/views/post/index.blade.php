<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Post') }}
        </h2>
    </x-slot>
    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <div>
        <div class="flex items-center justify-end px-4 py-3 text-right sm:px-6" style="text-align: end;">
            <a href="{{ URL::route('post.create') }}"
               type="button"
               class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                <i class="fas fa-plus"></i>
            </a>
        </div>
        @include('_messages')

        @if(count($posts) >0 )



            <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
                <div class="row">
                    @foreach($posts as $key => $post)
                        <div class="card mb-3 col-xl-5 col-md-6 mb-4 pl-0 pr-0 ml-3" style="max-width: 540px;">
                            <div class="row no-gutters min-height-100">
                                <div class="col-md-4">
                                    <img src="{{$post->image_url}}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-8 d-flex flex-column justify-content-between min-height-100">
                                    <div class="card-body">
                                        <h5 class="card-title">{{Str::limit($post->title,25,'...')}}</h5>
                                        <p class="card-text">{{Str::limit($post->description,100,'...')}}</p>
                                        <a href="{{route('post.show',$post)}}" style="text-decoration: none">
                                            <button type="button" class="btn btn-sm btn-outline-primary">Show</button>
                                        </a>
                                        @if( Auth::user()->can('edit',$post))
                                            <a href="{{route('post.edit',$post)}}" style="text-decoration: none">
                                                <button type="button" class="btn btn-sm btn-outline-success">Edit
                                                </button>
                                            </a>
                                        @endif
                                        @if(Auth::user()->can('delete',$post))
                                            <button type="button" data-toggle="modal" data-target="#confirm-delete"
                                                    class="btn btn-sm btn-outline-danger">Delete
                                            </button>
                                        @endif
                                    </div>
                                    <div class="card-footer row justify-content-between pl-2 pr-2 mr-0 mw-100 ml-0">
                                        <small class="text-muted">By {{$post->user->name}}</small>
                                        <p class="card-text"><small
                                                class="text-muted">{{$post->updated_at->format('Y-m-d')}}</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
            {{ $posts->links()  }}

        <!--- Modal -->
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            Are you sure?
                        </div>
                        <div class="modal-body">
                            You can't revert changes
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <form action="{{route('post.destroy',$post)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-ok" id="delete-post">
                                    Delete
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    </div>


</x-app-layout>


