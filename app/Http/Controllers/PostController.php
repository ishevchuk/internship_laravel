<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostCreate;
use App\Http\Requests\PostUpdate;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(4);
        return view('post.index')->with([
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreate $request)
    {
        try {
            $user = Auth::user();

            $input = $request->all();

            $post = new Post();
            $post->user_id = $user->id;

            $path = $request->file('image')->storePublicly('public/posts');
            $post->image = $path;

            $post->fill($input);
            $post->save();
            Session::flash('message', 'Item was created');
            return redirect()->route('post.index');
        } catch (\Exception $e) {
            Log::error($e);
            Session::flash('error','Something goes wrong');
            return redirect()->back()->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('post.show')->with([
            'post' => $post
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $user = Auth::user();
        if (!$user->can('delete',$post)) {
            Session::flash('error','You can edit only post that you own');
            return redirect()->route('post.index');
        }
        return view('post.edit')->with([
           'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostUpdate $request
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdate $request, Post $post)
    {
        try {
            $user = Auth::user();

            $input = $request->all();
            if (!$user->can('delete',$post)) {
                Session::flash('error','You can update only post that you own');
                return redirect()->route('post.index');
            }
                if ($request->file('image')){
                $path = $request->file('image')->storePublicly('public/posts');
                Storage::delete($post->image);
                $post->image = $path;
            }

            $post->update($input);
            Session::flash('message', 'Item was created');
            return redirect()->route('post.index');
        } catch (\Exception $e) {
            Log::error($e);
            Session::flash('error','Something goes wrong');
            return redirect()->back()->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        try {
            $user = Auth::user();
            if (!$user->can('delete',$post)){
                Session::flash('error','You can delete only post that you own');
                return redirect()->route('post.index');
            }
            $post->delete();
            Session::flash('message', 'Successfully deleted item');
            return redirect()->route('post.index');
        } catch (\Exception $e) {
            Log::error($e);
            Session::flash('error','Something goes wrong');
            return redirect()->back();
        }
    }
}
